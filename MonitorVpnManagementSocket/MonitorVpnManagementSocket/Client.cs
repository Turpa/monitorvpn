﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace MonitorVpnManagementSocket
{
    /// <summary>
    /// This
    /// </summary>
    public class Client
    {
        private string commonName;
        private string realAddress;
        private string virtualAddress;
        private string virtualIPv6Address;
        private string bytesReceived;
        private string bytesSent;
        private DateTime connectedSince;
        private string connectedSinceTime_t;
        private string username;
        private string clientID;
        private string peerID;

        public Client(List<string> client)
        {
            CommonName = client[1];
            RealAddress = client[2];
            VirtualAddress = client[3];
            VirtualIPv6Address = client[4];
            BytesReceived = client[5];
            BytesSent = client[6];
            ConnectedSince = Regex.IsMatch(client[7], @"^\w{4}-\w{2}-\w{2} \w{2}:\w{2}:\w{2}$", RegexOptions.Multiline) ? DateTime.Parse(client[7], new CultureInfo("en-US")) : DateTime.ParseExact(client[7], "ddd MMM dd HH:mm:ss yyyy", new CultureInfo("en-US"));
            ConnectedSinceTime_t = client[8];
            Username = client[9];
            ClientID = client[10];
            PeerID = client[11];
        }
        public void PrintClient()
        {

            DeleteTextCurrentLine.DeleteText();            

            Console.WriteLine("\rCommon Name:{0}", CommonName);
            Console.WriteLine("\rReal Address:{0}", RealAddress);
            Console.WriteLine("\rVirtual Address:{0}", VirtualAddress);
            Console.WriteLine("\rVirtual IPv6 Address:{0}", VirtualIPv6Address);
            Console.WriteLine("\rBytes Received:{0}", BytesReceived);
            Console.WriteLine("\rBytes Sent:{0}", BytesSent);
            Console.WriteLine("\rConnected Since:{0}", ConnectedSince);
            Console.WriteLine("\rConnected Since(time_t):{0}", connectedSinceTime_t);
            Console.WriteLine("\rUsername:{0}", Username);
            Console.WriteLine("\rClientID:{0}", ClientID);
            Console.WriteLine("\rPeerID:{0}", PeerID);
        }




        // Get list of all clients connected to vpn.
        private static List<string> GetAListOfCurrentConnectedClients()
        {
            // create temp list
            List<string> temp = new List<string>();
            // await LastEncoded information.
            //UNDONE change to function.
            //Stopwatch sW = new Stopwatch();
            //sW.Start();
            //StringBuilder stringBuilder = new StringBuilder();
            //do
            //{
            //    if (sW.Elapsed <= TimeSpan.FromSeconds(3))
            //        Console.Write("\r{0}", "waiting for client list.");
            //    Thread.Sleep(50);
            //    if (sW.Elapsed >= TimeSpan.FromSeconds(4) && sW.Elapsed <= TimeSpan.FromSeconds(6))
            //        Console.Write("\r{0}", "waiting for client list..");
            //    if (sW.Elapsed >= TimeSpan.FromSeconds(7))
            //        Console.Write("\r{0}", "waiting for client list...");


            //    if (sW.Elapsed >= TimeSpan.FromSeconds(10))
            //    {
            //        break;
            //    }

            //} while ( Properties.LastEncodedNetworkStream == null || !Properties.LastEncodedNetworkStream.ToString().Contains("CLIENT_LIST") );
            //sW.Stop();
            WaitForSocketToFinnishReading.WhileWaitForSocket("waiting for client list", "CLIENT_LIST") ;
            // Check if the last recived stream is a Status update. (Status update contains the connected clients.)
            if (Properties.LastEncodedNetworkStream.ToString().Contains("CLIENT_LIST"))
            {
                // Split encoded stream on all new lines and add to temp list.
                temp = Properties.LastEncodedNetworkStream.ToString().Split("\n").ToList();
                Properties.LastEncodedNetworkStream = null;
                //Properties.LastEncodedNetworkStream = new StringBuilder();
                // For every new line in temp list.
                for (int i = 0; i < temp.Count; i++)
                {
                    // Removes header from list.
                    if (temp[i].Contains("HEADER,CLIENT_LIST"))
                    {
                        temp.RemoveAt(i);
                        i--;
                    }

                    // Removes every line that's not a client connection.
                    else if (!temp[i].Contains("CLIENT_LIST"))
                    {
                        temp.RemoveAt(i);
                        i--;
                    }

                }
                //TODO Properties.ListOfClientsConnected Changed 2 times
                // temp is 0 no clients connected.
                //if (temp.Count == 0)
                //{
                //    // Update property.
                //    Properties.ListOfClientsConnected = new List<Client>();
                //}
              
            }
            return temp;
        }





        // Create a list of clients connected from string
        public static List<Client> GetClientsAsync()
        {
            
            // Send command Status 2 to socket.
            Program.Monitor.BeginSend("status 2");

            // Create a new client list.
            List<Client> clients = new List<Client>();

            // testing only....

            for (int i = 1; i <= 100000; i++)
            {
                clients.Add(new Client(new List<string>() { " ", "client" + i.ToString(), "10.0.2.2:54675", "10.8.0.6", " ", "38125", "6452", "Fri Jun 12 12:21:04 2020", "1591964464", "UNDEF", "0", "0" }));
            }

            Properties.ListOfClientsConnected = clients;
            //-------- testing end


            //await Properties.LastEncodedNetworkStream;
            List<string> stringClientList = GetAListOfCurrentConnectedClients();
            
            // Checks so list is not empty.
            if (stringClientList.Count != 0)
            {
                //Create List


                // Foreach new line in string from socket.
                foreach (var line in stringClientList)
                {
                    // create temporary list.
                    List<string> temp = new List<string>();
                    // split each line on every ','.
                    foreach (var item in line.Split(','))
                    {

                        // Add each item to temp list.
                        temp.Add(item.Replace("\r", ""));
                    }

                    // Add temp list to list of clients.
                    clients.Add(new Client(temp));
                }
                // Set list of clients to current connected clients
                //Properties.ListOfClientsConnected = null;
                
                Properties.ListOfClientsConnected = clients;

            }
            // clients is 0 no clients connected.
            else
            {
                //TODO Properties.ListOfClientsConnected Changed 2 times
                Properties.ListOfClientsConnected = new List<Client>();

            }
            return Properties.ListOfClientsConnected/*clients*/;
        }

        #region Setters
        public string CommonName
        {
            get
            {
                return commonName;
            }
            set
            {
                commonName = value;
            }
        }
        public string RealAddress
        {
            get
            {
                return realAddress;
            }
            set
            {
                realAddress = value;
            }
        }
        public string VirtualAddress
        {
            get
            {
                return virtualAddress;
            }
            set
            {
                virtualAddress = value;
            }
        }
        public string VirtualIPv6Address
        {
            get
            {
                return virtualIPv6Address;
            }
            set
            {
                virtualIPv6Address = value;
            }
        }
        public string BytesReceived
        {
            get
            {
                return bytesReceived;
            }
            set
            {
                bytesReceived = value;
            }
        }
        public string BytesSent
        {
            get
            {
                return bytesSent;
            }
            set
            {
                bytesSent = value;
            }
        }
        public DateTime ConnectedSince
        {
            get
            {
                return connectedSince;
            }
            set
            {
                connectedSince = value;
            }

        }
        public string ConnectedSinceTime_t
        {
            get
            {
                return connectedSinceTime_t;
            }
            set
            {
                connectedSinceTime_t = value;
            }
        }
        public string Username
        {
            get
            {
                return username;
            }
            set
            {
                username = value;
            }
        }
        public string ClientID
        {
            get
            {
                return clientID;
            }
            set
            {
                clientID = value;
            }
        }
        public string PeerID
        {
            get
            {
                return peerID;
            }
            set
            {
                peerID = value;
            }
        }
        #endregion setters
    }








}



