﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace MonitorVpnManagementSocket
{
    class WaitForSocketToFinnishReading
    {
        public static void WhileWaitForSocket(string aConsoleWrite)
        {

            Stopwatch s = new Stopwatch();
            s.Start();
            do
            {
                if (s.Elapsed <= TimeSpan.FromSeconds(3))
                    Console.Write("\r{0}.", aConsoleWrite);

                if (s.Elapsed >= TimeSpan.FromSeconds(4) && s.Elapsed <= TimeSpan.FromSeconds(6))
                    Console.Write("\r{0}..", aConsoleWrite);
                if (s.Elapsed >= TimeSpan.FromSeconds(7))
                    Console.Write("\r{0}...", aConsoleWrite);
                if (s.Elapsed >= TimeSpan.FromSeconds(10))
                {
                    break;
                }
                Thread.Sleep(50);
            } while (Properties.LastEncodedNetworkStream == null);
            s.Stop();
        }
        public static void WhileWaitForSocket(string aConsoleWrite, string containsWhat)
        {

            Stopwatch s = new Stopwatch();
            s.Start();
            do
            {
                if (s.Elapsed <= TimeSpan.FromSeconds(3))
                    Console.Write("\r{0}.", aConsoleWrite);

                if (s.Elapsed >= TimeSpan.FromSeconds(4) && s.Elapsed <= TimeSpan.FromSeconds(6))
                    Console.Write("\r{0}..", aConsoleWrite);
                if (s.Elapsed >= TimeSpan.FromSeconds(7))
                    Console.Write("\r{0}...", aConsoleWrite);
                if (s.Elapsed >= TimeSpan.FromSeconds(10))
                {
                    break;
                }
                Thread.Sleep(50);
            } while (Properties.LastEncodedNetworkStream == null || !Properties.LastEncodedNetworkStream.ToString().Contains(containsWhat));
            s.Stop();
        }
    }
}
