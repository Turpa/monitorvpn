﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Security.AccessControl;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace MonitorVpnManagementSocket
{
    class Properties
    {
        private static bool consoleWrite;
        private static StringBuilder lastEncodedNetworkStream;
        private static NetworkStream nStream;
        private static int networkStreamNumberOfBytesRead;
        private static Socket s;
        private static IPAddress ipAddress;
        private static IPEndPoint ipEndPoint;
        private static List<Client> listOfClientsConnected;
        private static byte[] buffer;
        private static byte[] sendBuffer;
        private static bool connected;
        private static int port;
        private static int autoUpdateClientDataIntervalSeconds;
        private static bool autoUpdateClientData;

        private static string user;
        private static string password;


        public static List<Client> ListOfClientsConnected
        {
            get { return listOfClientsConnected; }
            set
            {

                var sql = true;
                if (sql == true)
                {
                    //100k clients 158 sec.
                    Stopwatch s = new Stopwatch();
                    s.Start();
                    if(value.Count >= 1000)
                    {
                        SQL.DeleteClientsThatDisconnectedIfClientCountExceed2000(value);
                        SQL.InsertNewConnectedClientsIfClientCountExceed2000(value);
                    }
                    else
                    {
                        SQL.DeleteClientsThatDisconnected(value);

                        SQL.InsertNewConnectedClients(value);

                    }

                    s.Stop();
                    listOfClientsConnected = SQL.IndexConnectedClients();

                    Console.WriteLine(s.Elapsed.TotalSeconds ); 





                       
                 }
                else
                {

                    if (listOfClientsConnected == null || listOfClientsConnected.Count == 0 && value.Count > 0)
                    {
                        listOfClientsConnected = value;
                    }

                    else
                    {
                        int tempListOfClientsConnectedCount = listOfClientsConnected.Count;

                        for (var i = 0; i < tempListOfClientsConnectedCount; i++)
                        {
                            if (value.Count == 0 && tempListOfClientsConnectedCount > 0)
                            {
                                Console.WriteLine("{0} Disconnect", listOfClientsConnected[i].CommonName);
                                listOfClientsConnected.RemoveAt(i);
                            }
                            for (var x = 0; x < value.Count; x++)
                            {
                                if (listOfClientsConnected[i].CommonName == value[x].CommonName)
                                {
                                    // value[x] already connected 
                                    // update new values. time, bandwith etc.

                                    listOfClientsConnected[i].BytesSent = value[x].BytesSent;
                                    listOfClientsConnected[i].BytesReceived = value[x].BytesReceived;
                                    listOfClientsConnected[i].ConnectedSince = value[x].ConnectedSince;

                                    value.RemoveAt(x);

                                    break;


                                }
                                if (i == tempListOfClientsConnectedCount - 1)
                                {

                                    // value x new connection.
                                    listOfClientsConnected.Add(value[x]);
                                    value.RemoveAt(x);
                                    x--;
                                }
                                if (x == value.Count - 1)
                                {
                                    // i discconect
                                    listOfClientsConnected.RemoveAt(i);
                                    tempListOfClientsConnectedCount--;
                                    i--;
                                }

                            }
                        }


                    }


                }

            }
        }



        public static bool ConsoleWrite
        {
            get; set;
        } = true;

        public static StringBuilder LastEncodedNetworkStream
        {
            get { return lastEncodedNetworkStream; }
            set
            {
                lastEncodedNetworkStream = value;
            }
        }

        public static byte[] Buffer
        {
            get { return buffer; }
            set
            {
                buffer = value;
            }
        }
        public static byte[] SendBuffer
        {
            get { return sendBuffer; }
            set
            {
                sendBuffer = value;
            }
        }
        public static bool Connected
        {
            get { return connected; }
            set
            {
                connected = value;
            }
        }
        public static IPEndPoint IpEndPoint
        {
            get { return ipEndPoint; }
            set
            {
                ipEndPoint = value;
            }
        }
        public static NetworkStream NStream
        {
            get { return nStream; }
            set
            {
                nStream = value;
            }
        }
        public static int NetworkStreamNumberOfBytesRead
        {
            get { return networkStreamNumberOfBytesRead; }
            set
            {
                networkStreamNumberOfBytesRead = value;
            }
        }
        public static Socket S
        {
            get { return s; }
            set
            {

                s = value;

            }
        }

        public static IPAddress IpAddress
        {
            get { return ipAddress; }
            set
            {
                ipAddress = value;
            }
        }
        public static int Port
        {
            get { return port; }
            set
            {
                if (value > 0 && value <= 65535)
                {
                    port = value;
                }
                else
                {
                    Console.WriteLine("Error App.config:Invalid port {0}!\n\rValid port range between 1-65535.\n\nTerminating Application.\nPress < Any key > to continue.", value);
                    Console.Read();
                    Environment.Exit(1);
                }
            }
        }

        public static int AutoUpdateClientDataIntervalSeconds 
        {
            get { return autoUpdateClientDataIntervalSeconds; }
            set
            {
                if (value == 0) { autoUpdateClientData = false; autoUpdateClientDataIntervalSeconds = 0; return; }
                autoUpdateClientDataIntervalSeconds = value;
                autoUpdateClientData = true;

            }
        }
        public static bool AutoUpdateClientData
        {
            get { return autoUpdateClientData; }
        }

    }
}
