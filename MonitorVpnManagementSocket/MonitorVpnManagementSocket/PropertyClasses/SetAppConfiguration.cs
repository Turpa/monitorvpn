﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Text;

namespace MonitorVpnManagementSocket
{
    class SetAppConfiguration
    {

        public static void SetIpAddress()
        {
            try
            {
                Properties.IpAddress = IPAddress.Parse(ConfigurationManager.AppSettings["ipaddress"]);
            }
            catch (Exception eX)
            {

                Console.Write("Error App.config:Invaild ipaddress.\nException:{0}\n\nTerminating Application.\nPress <Any key> to continue.", eX.Message);
                Console.Read();
                Environment.Exit(1);
            }
        }

        public static void SetPort()
        {
            try
            {
                Properties.Port = int.Parse(ConfigurationManager.AppSettings["port"]);
                //int port = int.Parse(ConfigurationManager.AppSettings["port"]);

            }
            catch (Exception eX)
            {

                Console.Write("Error App.config:Invaild port.\nException:{0}\n\nTerminating Application.\nPress <Any key> to continue.", eX.Message);
                Console.Read();
                Environment.Exit(1);

            }
        }
   

        public static void SetAutoGetclients()
        {
            try
            {
                Properties.AutoUpdateClientDataIntervalSeconds = int.Parse(ConfigurationManager.AppSettings["autoupdateclientdata"]);
            }
            catch(Exception eX)
            {
                Console.Write("Error App.config:Invaild input in auto update client.\nException:{0}\n\nTerminating Application.\nPress <Any key> to continue.", eX.Message);
                Console.Read();
                Environment.Exit(1);
            }
        }


        public static void SetAppConfigurations()
        {
            SetIpAddress();
            SetPort();
            SetAutoGetclients();
        }

    }
}
