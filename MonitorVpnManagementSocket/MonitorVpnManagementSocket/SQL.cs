﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace MonitorVpnManagementSocket
{
    class SQL
    {
        public static bool IsClientConnected(Client client)
        {


            using (SqlConnection sqlConnection = new SqlConnection(GetSqlConnectionStringFromAppConfig()))
            {
                string commandText = @"SELECT CommonName FROM Clients WHERE CommonName = @CommonName;";
                using (SqlCommand command = new SqlCommand(commandText, sqlConnection))
                {
                    
                    command.Parameters.Add("@CommonName", SqlDbType.VarChar, 50);
                    command.Parameters["@CommonName"].Value = client.CommonName;
                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {

                        if (dataReader.HasRows)
                        {
                            return true;
                        }

                    }
                }

            }
            return false;

        }

        public static List<Client> IndexConnectedClients()
        {
     

            using (SqlConnection sqlConnection = new SqlConnection(GetSqlConnectionStringFromAppConfig()))
            {
                

                string commandText = @"SELECT * FROM Clients;";
                using (SqlCommand command = new SqlCommand(commandText, sqlConnection))
                {

                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        if (!dataReader.HasRows) return null;

                        List<Client> tempListOfClients = new List<Client>();
                                                
                                    
                        while (dataReader.Read())
                        {
                                    
                                    
                            Client tempClient = new Client(new List<string>() 
                            {   "",
                                dataReader["CommonName"].ToString(),
                                dataReader["RealAddress"].ToString(),
                                dataReader["VirtualAddress"].ToString(),
                                dataReader["VirtualIPv6Address"].ToString(),
                                dataReader["BytesReceived"].ToString(),
                                dataReader["BytesSent"].ToString(),
                                dataReader["ConnectedSince"].ToString(),
                                dataReader["ConnectedSinceTime_t"].ToString(),
                                dataReader["Username"].ToString(),
                                dataReader["ClientID"].ToString(),
                                dataReader["PeerID"].ToString()

                            });
                            tempListOfClients.Add(tempClient);
                        }

                        return tempListOfClients;
                        

                        
                    }

                }

            }
            
        }

        public static int DeleteClientsThatDisconnectedIfClientCountExceed2000(List<Client> clients)
        {


            using (SqlConnection sqlConnection = new SqlConnection(GetSqlConnectionStringFromAppConfig()))
            {

                //string[] paramArray = clients.Select((client, index) => "@CommonName" + index.ToString()).ToArray();
                //string inClause = string.Join(",", paramArray);
                //string commandText = String.Format(@"DELETE FROM Clients WHERE CommonName NOT IN ({0});", inClause);
                string commandText = string.Empty;



                using (SqlCommand command = new SqlCommand(commandText, sqlConnection))
                {
                    sqlConnection.Open();
                    int deleted = 0;
                    command.CommandText = @"TRUNCATE TABLE TempClients; DBCC CHECKIDENT ('TempClients', RESEED, 1);";
                    command.ExecuteNonQuery();
                                       
                    
                    var splitClientLists = SQL.SplitList(clients, 1000);

                    foreach (var splitClientList in splitClientLists)
                    {
                        clients = splitClientList;
                        
                        command.Parameters.Clear();
                        string[] paramArray = clients.Select((client, index) => "(" + "@CommonName" + index.ToString() + ")").ToArray();
                        string inClause = string.Join(",", paramArray);
                        command.CommandText = String.Format(@"INSERT INTO TempClients (CommonName) VALUES{0};", inClause);

                        for (int i = 0; i < paramArray.Length; i++)
                        {
                            command.Parameters.AddWithValue(paramArray[i].Replace("(", "").Replace(")", ""), clients[i].CommonName);
                        };

                        command.ExecuteNonQuery();


                    }

                    command.CommandText = @"DELETE FROM Clients WHERE CommonName NOT IN (SELECT CommonName FROM TempClients)"; 
                    deleted = command.ExecuteNonQuery();



                    return deleted;
                }

            }


        }
        public static int DeleteClientsThatDisconnected(List<Client> clients)
        {
            

            using (SqlConnection sqlConnection = new SqlConnection(GetSqlConnectionStringFromAppConfig()))
            {

                //string[] paramArray = clients.Select((client, index) => "@CommonName" + index.ToString()).ToArray();
                //string inClause = string.Join(",", paramArray);
                //string commandText = String.Format(@"DELETE FROM Clients WHERE CommonName NOT IN ({0});", inClause);
                string commandText = string.Empty;



                using (SqlCommand command = new SqlCommand(commandText, sqlConnection))
                {
                    sqlConnection.Open();
                    int deleted = 0;
                    if (clients == null || clients.Count == 0) { command.CommandText = @"TRUNCATE TABLE Clients; DBCC CHECKIDENT ('Clients', RESEED, 1);"; deleted = command.ExecuteNonQuery(); return deleted; }
                    else
                    {
                        //var listOfListsClient = SplitList(clients, 2000);
                        //foreach (var listOfListClient in listOfListsClient)
                        //{
                            //clients = listOfListClient;
                            command.Parameters.Clear();
                            string[] paramArray = clients.Select((client, index) => "@CommonName" + index.ToString()).ToArray();
                            string inClause = string.Join(",", paramArray);
                            command.CommandText = String.Format(@"DELETE FROM Clients WHERE CommonName NOT IN ({0});", inClause);

                            for (int i = 0; i < paramArray.Length; i++)
                            {
                                command.Parameters.AddWithValue(paramArray[i], clients[i].CommonName);
                            };

                            deleted += command.ExecuteNonQuery();
                        //}


                    }


                        return deleted;
                } 
                
            }
            

        }

        public static int InsertNewConnectedClientsIfClientCountExceed2000(List<Client> clients)
        {
            if (clients == null || clients.Count == 0) return 0;





            using (SqlConnection sqlConnection = new SqlConnection(GetSqlConnectionStringFromAppConfig()))
            {



                //string commandText = @"SELECT * FROM Clients WHERE CommonName IN ({0});";
                //string[] paramArray = clients.Select((client, index) => "@CommonName" + index.ToString()).ToArray();
                //commandText = string.Format(commandText, string.Join(",", paramArray));
                string commandText = string.Empty;
                using (SqlCommand command = new SqlCommand(commandText, sqlConnection))
                {


                    //string[] paramArray = clients.Select((client, index) => "@CommonName" + index.ToString()).ToArray();
                    //command.CommandText= string.Format(@"SELECT * FROM Clients WHERE CommonName IN ({0});", string.Join(",", paramArray));

                    //for (int i = 0; i < paramArray.Length; i++)
                    //{
                    //    command.Parameters.AddWithValue(paramArray[i], clients[i].CommonName);
                    //}
                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    command.CommandText = @"SELECT * FROM Clients WHERE CommonName IN (SELECT CommonName FROM TempClients);";

                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {


                                for (int i = 0; i < clients.Count; i++)
                                {
                                    if (dataReader["CommonName"].ToString() == clients[i].CommonName)
                                    {
                                        clients.RemoveAt(i);
                                    }
                                }

                            }
                        }
                    }


                    Console.WriteLine(clients.Count);


                    var listOfListsClient = SplitList(clients, 180);


                    if (clients.Count == 0) return 0;
                    int linesEffected = 0;
                    foreach (var listOfListClient in listOfListsClient)
                    {
                        command.Parameters.Clear();
                        clients = listOfListClient;

                        string[] paramArray = clients.Select((client, index) => "(" + "@CommonName" + index.ToString() + "," + "@RealAddress" + index.ToString() + "," + "@VirtualAddress" + index.ToString() + "," + "@VirtualIPv6Address" + index.ToString() + "," + "@BytesReceived" + index.ToString() + "," +
                        "@BytesSent" + index.ToString() + "," + "@ConnectedSince" + index.ToString() + "," + "@ConnectedSinceTime_t" + index.ToString() + "," + "@Username" + index.ToString() + "," + "@ClientID" + index.ToString() + "," + "@PeerID" + index.ToString() + ")").ToArray();
                        commandText = @"INSERT INTO Clients(CommonName, RealAddress, VirtualAddress, VirtualIPv6Address, BytesReceived, BytesSent, ConnectedSince, ConnectedSinceTime_t, Username, ClientID, PeerID) VALUES{0};";
                        command.CommandText = string.Format(commandText, string.Join(",", paramArray));

                        for (int i = 0; i < paramArray.Length; i++)
                        {
                            for (int paramI = 0; paramI < paramArray[i].Split(",").Length; paramI++)
                            {
                                string tempstring = string.Empty;
                                if (paramI == 0) tempstring = clients[i].CommonName;
                                if (paramI == 1) tempstring = clients[i].RealAddress;
                                if (paramI == 2) tempstring = clients[i].VirtualAddress;
                                if (paramI == 3) tempstring = clients[i].VirtualIPv6Address;
                                if (paramI == 4) tempstring = clients[i].BytesReceived;
                                if (paramI == 5) tempstring = clients[i].BytesSent;
                                if (paramI == 6) tempstring = clients[i].ConnectedSince.ToString();
                                if (paramI == 7) tempstring = clients[i].ConnectedSinceTime_t;
                                if (paramI == 8) tempstring = clients[i].Username;
                                if (paramI == 9) tempstring = clients[i].ClientID;
                                if (paramI == 10) tempstring = clients[i].PeerID;

                                command.Parameters.AddWithValue(paramArray[i].Split(",")[paramI].Replace("(", "").Replace(")", ""), tempstring);
                            }


                        }

                        linesEffected += command.ExecuteNonQuery();
                    }
                    //if (clients.Count == 0) return 0;
                    //paramArray = clients.Select((client, index) => "(" + "@CommonName" + index.ToString() + "," + "@RealAddress" + index.ToString() + "," + "@VirtualAddress" + index.ToString() + "," + "@VirtualIPv6Address" + index.ToString() + "," + "@BytesReceived" + index.ToString() + "," +
                    //"@BytesSent" + index.ToString() + "," + "@ConnectedSince" + index.ToString() + "," + "@ConnectedSinceTime_t" + index.ToString() + "," + "@Username" + index.ToString() + "," + "@ClientID" + index.ToString() + "," + "@PeerID" + index.ToString() + ")").ToArray();
                    //commandText = @"INSERT INTO Clients(CommonName, RealAddress, VirtualAddress, VirtualIPv6Address, BytesReceived, BytesSent, ConnectedSince, ConnectedSinceTime_t, Username, ClientID, PeerID) VALUES{0};";
                    //command.CommandText = string.Format(commandText, string.Join(",", paramArray));
                    //command.Parameters.Clear();
                    //for (int i = 0; i < paramArray.Length; i++)
                    //{
                    //    for (int paramI = 0; paramI < paramArray[i].Split(",").Length; paramI++)
                    //    {
                    //        string tempstring = string.Empty;
                    //        if (paramI == 0) tempstring = clients[i].CommonName;
                    //        if (paramI == 1) tempstring = clients[i].RealAddress;
                    //        if (paramI == 2) tempstring = clients[i].VirtualAddress;
                    //        if (paramI == 3) tempstring = clients[i].VirtualIPv6Address;
                    //        if (paramI == 4) tempstring = clients[i].BytesReceived;
                    //        if (paramI == 5) tempstring = clients[i].BytesSent;
                    //        if (paramI == 6) tempstring = clients[i].ConnectedSince.ToString();
                    //        if (paramI == 7) tempstring = clients[i].ConnectedSinceTime_t;
                    //        if (paramI == 8) tempstring = clients[i].Username;
                    //        if (paramI == 9) tempstring = clients[i].ClientID;
                    //        if (paramI == 10) tempstring = clients[i].PeerID;

                    //        command.Parameters.AddWithValue(paramArray[i].Split(",")[paramI].Replace("(", "").Replace(")", ""), tempstring);
                    //    }


                    //}


                    return linesEffected;
                }

            }





        }
        public static int InsertNewConnectedClients(List<Client> clients)
        {
            if (clients == null || clients.Count == 0) return 0;





            using (SqlConnection sqlConnection = new SqlConnection(GetSqlConnectionStringFromAppConfig()))
            {



                //string commandText = @"SELECT * FROM Clients WHERE CommonName IN ({0});";
                //string[] paramArray = clients.Select((client, index) => "@CommonName" + index.ToString()).ToArray();
                //commandText = string.Format(commandText, string.Join(",", paramArray));
                string commandText = string.Empty;
                using (SqlCommand command = new SqlCommand(commandText, sqlConnection))
                {


                    string[] paramArray = clients.Select((client, index) => "@CommonName" + index.ToString()).ToArray();
                    command.CommandText = string.Format(@"SELECT * FROM Clients WHERE CommonName IN ({0});", string.Join(",", paramArray));

                    for (int i = 0; i < paramArray.Length; i++)
                    {
                        command.Parameters.AddWithValue(paramArray[i], clients[i].CommonName);
                    }
                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    

                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {


                                for (int i = 0; i < clients.Count; i++)
                                {
                                    if (dataReader["CommonName"].ToString() == clients[i].CommonName)
                                    {
                                        clients.RemoveAt(i);
                                    }
                                }

                            }
                        }
                    }


                


                    var listOfListsClient = SplitList(clients, 180);


                    if (clients.Count == 0) return 0;
                    int linesEffected = 0;
                    foreach (var listOfListClient in listOfListsClient)
                    {
                        command.Parameters.Clear();
                        clients = listOfListClient;

                        paramArray = clients.Select((client, index) => "(" + "@CommonName" + index.ToString() + "," + "@RealAddress" + index.ToString() + "," + "@VirtualAddress" + index.ToString() + "," + "@VirtualIPv6Address" + index.ToString() + "," + "@BytesReceived" + index.ToString() + "," +
                        "@BytesSent" + index.ToString() + "," + "@ConnectedSince" + index.ToString() + "," + "@ConnectedSinceTime_t" + index.ToString() + "," + "@Username" + index.ToString() + "," + "@ClientID" + index.ToString() + "," + "@PeerID" + index.ToString() + ")").ToArray();
                        commandText = @"INSERT INTO Clients(CommonName, RealAddress, VirtualAddress, VirtualIPv6Address, BytesReceived, BytesSent, ConnectedSince, ConnectedSinceTime_t, Username, ClientID, PeerID) VALUES{0};";
                        command.CommandText = string.Format(commandText, string.Join(",", paramArray));

                        for (int i = 0; i < paramArray.Length; i++)
                        {
                            for (int paramI = 0; paramI < paramArray[i].Split(",").Length; paramI++)
                            {
                                string tempstring = string.Empty;
                                if (paramI == 0) tempstring = clients[i].CommonName;
                                if (paramI == 1) tempstring = clients[i].RealAddress;
                                if (paramI == 2) tempstring = clients[i].VirtualAddress;
                                if (paramI == 3) tempstring = clients[i].VirtualIPv6Address;
                                if (paramI == 4) tempstring = clients[i].BytesReceived;
                                if (paramI == 5) tempstring = clients[i].BytesSent;
                                if (paramI == 6) tempstring = clients[i].ConnectedSince.ToString();
                                if (paramI == 7) tempstring = clients[i].ConnectedSinceTime_t;
                                if (paramI == 8) tempstring = clients[i].Username;
                                if (paramI == 9) tempstring = clients[i].ClientID;
                                if (paramI == 10) tempstring = clients[i].PeerID;

                                command.Parameters.AddWithValue(paramArray[i].Split(",")[paramI].Replace("(", "").Replace(")", ""), tempstring);
                            }


                        }

                        linesEffected += command.ExecuteNonQuery();
                    }
                    //if (clients.Count == 0) return 0;
                    //paramArray = clients.Select((client, index) => "(" + "@CommonName" + index.ToString() + "," + "@RealAddress" + index.ToString() + "," + "@VirtualAddress" + index.ToString() + "," + "@VirtualIPv6Address" + index.ToString() + "," + "@BytesReceived" + index.ToString() + "," +
                    //"@BytesSent" + index.ToString() + "," + "@ConnectedSince" + index.ToString() + "," + "@ConnectedSinceTime_t" + index.ToString() + "," + "@Username" + index.ToString() + "," + "@ClientID" + index.ToString() + "," + "@PeerID" + index.ToString() + ")").ToArray();
                    //commandText = @"INSERT INTO Clients(CommonName, RealAddress, VirtualAddress, VirtualIPv6Address, BytesReceived, BytesSent, ConnectedSince, ConnectedSinceTime_t, Username, ClientID, PeerID) VALUES{0};";
                    //command.CommandText = string.Format(commandText, string.Join(",", paramArray));
                    //command.Parameters.Clear();
                    //for (int i = 0; i < paramArray.Length; i++)
                    //{
                    //    for (int paramI = 0; paramI < paramArray[i].Split(",").Length; paramI++)
                    //    {
                    //        string tempstring = string.Empty;
                    //        if (paramI == 0) tempstring = clients[i].CommonName;
                    //        if (paramI == 1) tempstring = clients[i].RealAddress;
                    //        if (paramI == 2) tempstring = clients[i].VirtualAddress;
                    //        if (paramI == 3) tempstring = clients[i].VirtualIPv6Address;
                    //        if (paramI == 4) tempstring = clients[i].BytesReceived;
                    //        if (paramI == 5) tempstring = clients[i].BytesSent;
                    //        if (paramI == 6) tempstring = clients[i].ConnectedSince.ToString();
                    //        if (paramI == 7) tempstring = clients[i].ConnectedSinceTime_t;
                    //        if (paramI == 8) tempstring = clients[i].Username;
                    //        if (paramI == 9) tempstring = clients[i].ClientID;
                    //        if (paramI == 10) tempstring = clients[i].PeerID;

                    //        command.Parameters.AddWithValue(paramArray[i].Split(",")[paramI].Replace("(", "").Replace(")", ""), tempstring);
                    //    }


                    //}


                    return linesEffected;
                }

            }





        }

        public static List<List<Client>> SplitList(List<Client> locations, int nSize = 30)
        {
            var list = new List<List<Client>>();
            for (int i = 0; i < locations.Count; i += nSize)
            {
                list.Add(locations.GetRange(i, Math.Min(nSize, locations.Count - i)));
            }
            return list;
        }
        //public static IEnumerable<List<Client>> SplitList<Client>(List<Client> locations, int nSize = 30)
        //{
        //    for (int i = 0; i < locations.Count; i += nSize)
        //    {
        //        yield return locations.GetRange(i, Math.Min(nSize, locations.Count - i));
        //    }
        //}

        public static void InsertSQLData()
        {
            using (SqlConnection sqlConnection= new SqlConnection(GetSqlConnectionStringFromAppConfig()))
            {
                SqlParameter sqlParameter = new SqlParameter();
                string commandText = "INSERT INTO Clients (CommonName) VALUES ('client2');";
                //string commandText = "DELETE FROM Clients where CommonName='client2';";
                //string commandText = "DELETE FROM Clients where ID=7;";


                using (SqlCommand command = new SqlCommand(commandText ,sqlConnection))
                {
                    sqlConnection.Open();
                    command.ExecuteNonQuery();
                }
            }

            Console.WriteLine("done");
            
        }

        public static void UpdateSQLData()
        {

            //string commandText = "UPDATE Clients SET RealAddress='10.0.2.2:50868' where CommonName = @client;";
            //string commandText = "DELETE FROM Clients where CommonName='client2';";
            string commandText = "DELETE FROM Clients where ID=6;";

            using (SqlConnection sqlConnection = new SqlConnection(GetSqlConnectionStringFromAppConfig()))
            {

                using (SqlCommand command = new SqlCommand(commandText, sqlConnection))
                {
                    SqlParameter parm = new SqlParameter();
                    command.Parameters.Add("@client", SqlDbType.VarChar, 100);
                    command.Parameters["@client"].Value = "Client2";
                    sqlConnection.Open();
                    command.ExecuteNonQuery();
                }
            }

            Console.WriteLine("done");
           

        }


        public static string GetSqlConnectionStringFromAppConfig()
        {
            string hostNameSQL = string.Empty;
            string serverNameSQL = string.Empty;
            string databaseNameSQL = string.Empty;
            string userNameSQL = string.Empty;
            string passwordSQL = string.Empty;

            try
            {
                hostNameSQL = ConfigurationManager.AppSettings["hostnameSQL"];
            }
            catch (Exception eX)
            {
                Console.Write("Error App.config:Invaild input in auto update client.\nException:{0}\n\nTerminating Application.\nPress <Any key> to continue.", eX.Message);
                Console.Read();
                Environment.Exit(1);
            }
            try
            {
                serverNameSQL = ConfigurationManager.AppSettings["servernameSQL"];
            }
            catch (Exception eX)
            {
                Console.Write("Error App.config:Invaild input in auto update client.\nException:{0}\n\nTerminating Application.\nPress <Any key> to continue.", eX.Message);
                Console.Read();
                Environment.Exit(1);
            }
            

            try
            {
                databaseNameSQL = ConfigurationManager.AppSettings["databasenameSQL"];
            }
            catch (Exception eX)
            {
                Console.Write("Error App.config:Invaild input in auto update client.\nException:{0}\n\nTerminating Application.\nPress <Any key> to continue.", eX.Message);
                Console.Read();
                Environment.Exit(1);
            }

            try
            {
                userNameSQL = ConfigurationManager.AppSettings["usernameSQL"];
            }
            catch (Exception eX)
            {
                Console.Write("Error App.config:Invaild input in auto update client.\nException:{0}\n\nTerminating Application.\nPress <Any key> to continue.", eX.Message);
                Console.Read();
                Environment.Exit(1);
            }
            try
            {
                passwordSQL = ConfigurationManager.AppSettings["passwordSQL"];
            }
            catch (Exception eX)
            {
                Console.Write("Error App.config:Invaild input in auto update client.\nException:{0}\n\nTerminating Application.\nPress <Any key> to continue.", eX.Message);
                Console.Read();
                Environment.Exit(1);
            }

            string.Format("{0}", "hej","då");

            return string.Format(@"Data Source={0}\{1};Initial Catalog={2};User ID={3};Password={4}", hostNameSQL, serverNameSQL, databaseNameSQL, userNameSQL, passwordSQL);
            
        }
    }
}
