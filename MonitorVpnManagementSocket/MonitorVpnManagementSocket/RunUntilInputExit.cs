﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace MonitorVpnManagementSocket
{
    class RunUntilInputExit
    {

        // Console wait for command to send to socket.

        
        //public static List<Task> taskWaitforBeginReadTofinnish = new List<Task>();


        
        public static void BeginReadingForEver()
        {
            
            while (1 == 1)
            {

                if (Properties.NStream.DataAvailable)
                {
                   
                    Program.Monitor.BeginReading();
                    
                }
                Thread.Sleep(500);
                
            }
        }

        public static void LoopUntilInputExit(string[] args)
        {
             Properties.LastEncodedNetworkStream = null;
             Task.Run(()=>BeginReadingForEver());

            Stopwatch s = new Stopwatch();
            s.Start();
            do
            {
                if (s.Elapsed <= TimeSpan.FromSeconds(3))
                    Console.Write("\r{0}", "Waiting for socket.");

                if (s.Elapsed >= TimeSpan.FromSeconds(4) && s.Elapsed <= TimeSpan.FromSeconds(6))
                    Console.Write("\r{0}", "Waiting for socket..");
                if (s.Elapsed >= TimeSpan.FromSeconds(7))
                    Console.Write("\r{0}", "Waiting for socket...");
                if (s.Elapsed >= TimeSpan.FromSeconds(10))
                {
                    break;
                }
                Thread.Sleep(50);
            } while ( Properties.LastEncodedNetworkStream == null );
            s.Stop();



            bool exit = false;
             // While exit is false.
             while (exit == false)
             {

                // wait for console input.
                Console.Write(">");
                string command = Console.ReadLine();


                

                if (command.ToLower() == "exit")
                {
                    // input = exit exist program.
                    break;
                }
                else if (command.ToLower() == "getclients")
                {
                    Properties.ConsoleWrite = false;
                    var clients = Client.GetClientsAsync();

                    if (clients.Count != 0) 
                    { 
                    foreach (var client in clients)
                    {

                        client.PrintClient();
                    }
                    }
                    else
                    {
                        Console.SetCursorPosition(0, Console.CursorTop);
                        Console.Write(new String(' ', Console.WindowWidth));
                        Console.SetCursorPosition(0, Console.CursorTop);
                        Console.WriteLine("There are 0 clients currently connected.");
                        
                    }

                    

                }
                else if (command.ToLower() == "help")
                {
                    Program.Monitor.BeginSend(command);
                    Properties.ConsoleWrite = false;
                    Stopwatch sW = new Stopwatch();
                    sW.Start();
                    do
                    {
                        if (sW.Elapsed <= TimeSpan.FromSeconds(3))
                            Console.Write("\r{0}", "Waiting for socket.");
                        
                        if (sW.Elapsed >= TimeSpan.FromSeconds(4) && sW.Elapsed <= TimeSpan.FromSeconds(6))
                            Console.Write("\r{0}", "Waiting for socket..");
                        if (sW.Elapsed >= TimeSpan.FromSeconds(7))
                            Console.Write("\r{0}", "Waiting for socket...");
                        if (sW.Elapsed >= TimeSpan.FromSeconds(10))
                        {
                            break;
                        }
                        Thread.Sleep(50);
                    } while (Properties.LastEncodedNetworkStream == null || !Properties.LastEncodedNetworkStream.ToString().Contains("Commands:"));
                    sW.Stop();

                    //Console.Clear();

                    Console.SetCursorPosition(0, Console.CursorTop);
                    Console.Write(new String(' ', Console.WindowWidth));
                    Console.SetCursorPosition(0, Console.CursorTop);
                    //Properties.LastEncodedNetworkStream.Clear(); 
                    Console.WriteLine(Regex.Replace("\r" + Properties.LastEncodedNetworkStream.ToString(), @"^END\s+$", "\r\nMonitor commands:\r\ngetclients             : Get a list of clients\r\nEND", RegexOptions.Multiline ));

                    Properties.LastEncodedNetworkStream = null;
                }
                else if(command == string.Empty)
                {
                    
                }
                else
                    {
                    Stopwatch sW = new Stopwatch();
                    sW.Start();
                    Properties.LastEncodedNetworkStream = null;
                    // input is not exit, send to socket.
                    Program.Monitor.BeginSend(command);
                    Properties.ConsoleWrite = false;
                    do
                    {
                        if (sW.Elapsed <= TimeSpan.FromSeconds(3))
                            Console.Write("\r{0}", "Waiting for socket.");

                        if (sW.Elapsed >= TimeSpan.FromSeconds(4) && sW.Elapsed <= TimeSpan.FromSeconds(6))
                            Console.Write("\r{0}", "Waiting for socket..");
                        if (sW.Elapsed >= TimeSpan.FromSeconds(7))
                            Console.Write("\r{0}", "Waiting for socket...");
                        if (sW.Elapsed >= TimeSpan.FromSeconds(10))
                        {
                            break;
                        }
                    } while (Properties.LastEncodedNetworkStream == null);
                    
                    sW.Stop();
                    Console.SetCursorPosition(0, Console.CursorTop);
                    Console.Write(new String(' ', Console.WindowWidth));
                    Console.SetCursorPosition(0, Console.CursorTop);
                    Console.Write("\r{0}", Properties.LastEncodedNetworkStream);

                }

             }
            
        }
    }
}
