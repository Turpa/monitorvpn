﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace MonitorVpnManagementSocket
{
    class ReadCommandFromConsoleAndExecute
    {

        
        public static void WaitForCommandInput()
        {


            WaitForSocketToFinnishReading.WhileWaitForSocket("");
            DeleteTextCurrentLine.DeleteText();
            

            bool exit = false;
            // While exit is false.
            while (exit == false)
            {


                // wait for console input.
                Console.Write(">");
                string command = Console.ReadLine();




                if (command.ToLower() == "quit" || command.ToLower() == "exit")
                {
                    Console.Clear();
                    RunUntilInputExit.stopReading = true;
                    Properties.NStream.Close(2000);
                    Properties.S.Close();
                    string message = "Socket connection closed";
                    string termenatingMessage = "Program terminate in ";

                    Console.WriteLine("{0}", new String('=', Console.WindowWidth));
                    Console.WriteLine("{0}{1}{0}", new String('=', (Console.WindowWidth / 2 - " Info ".Length / 2)), " Info ");

                    Console.WriteLine("\n{0}{1}", new String(' ', (Console.WindowWidth / 2 - message.Length / 2 - 5)), new string('=', message.Length + 10));
                    Console.WriteLine("{0}{1}{2}{1}", new String(' ', (Console.WindowWidth / 2 - message.Length / 2 - 5)), new String('=', 5), message);
                    Console.WriteLine("{0}{1}\n", new String(' ', (Console.WindowWidth / 2 - message.Length / 2 - 5)), new string('=', message.Length + 10));


                    for (int i = 3; i > 0; i--)
                    {
                        Console.WriteLine("{0}{3}{1}{2}{3}", new string(' ', (Console.WindowWidth / 2 - (termenatingMessage.Length + 1) / 2 - 6)), termenatingMessage, i, new string('=', 6));
                        Console.Write("{0}{1}", new string(' ', (Console.WindowWidth / 2 - (termenatingMessage.Length + 1) / 2 - 6)), new string('=', termenatingMessage.Length + 13));

                        Console.Write("\n\n\n\n\n\n\n{0}\n", new string('=', Console.WindowWidth));
                        Console.Write(new string('=', Console.WindowWidth));

                        Thread.Sleep(1000);


                        //DeleteTextCurrentLine.DeleteText();
                        Console.SetCursorPosition(0, Console.CursorTop - 11);
                        //DeleteTextCurrentLine.DeleteText();

                    }

                    Environment.Exit(0);
                }
                else if (command.ToLower() == "getclients")
                {
                    Properties.ConsoleWrite = false;
                    var clients = Client.GetClientsAsync();

                    if (clients == null || clients.Count == 0)
                    {
                        //Delete text thats printed.
                        DeleteTextCurrentLine.DeleteText();

                        // Console, no clients connected.
                        Console.WriteLine("There are 0 clients currently connected.");
                    }
                    else
                    {

                        foreach (var client in clients)
                        {

                            client.PrintClient();
                        }
                    }



                }
                else if (command.ToLower() == "help")
                {
                    Program.Monitor.BeginSend(command);

                    Properties.ConsoleWrite = false;
                    WaitForSocketToFinnishReading.WhileWaitForSocket("Waiting for socket", "Commands:");


                    //UNDONE Can this be done in separate function2
                    DeleteTextCurrentLine.DeleteText();
                    
                    //UNDONE Make Comment.
                    Console.WriteLine(Regex.Replace("\r" + Properties.LastEncodedNetworkStream.ToString(), @"^END\s+$",
                        "\r\nMonitor commands:\r\ngetclients             : Get a list of clients\r\ncls                    : Clear console.\r\nEND", RegexOptions.Multiline));
                    
                    //UNDONE Make Comment.
                    Properties.LastEncodedNetworkStream = null;
                }
                else if(command.ToLower() == "cls")
                {
                    Console.Clear();
                }
                
                else if (command == string.Empty)
                {
                    // Do Nothing.
                }
                else
                {
                    //UNDONE Can this be done in separate function?

                    
                    Properties.LastEncodedNetworkStream = null;

                    // input is not exit, send to socket.
                    Program.Monitor.BeginSend(command);
                    //Properties.ConsoleWrite = false;


                    WaitForSocketToFinnishReading.WhileWaitForSocket("Waiting for socket");

                    DeleteTextCurrentLine.DeleteText();
                    //Console.Write("\r{0}", Properties.LastEncodedNetworkStream);

                }

            }

        }

    }
}
