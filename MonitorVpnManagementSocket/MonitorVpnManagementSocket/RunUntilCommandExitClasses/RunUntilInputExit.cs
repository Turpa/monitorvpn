﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace MonitorVpnManagementSocket
{
    class RunUntilInputExit
    {

        public static bool stopReading = false;
 
        public static void BeginReadingForEver()
        {
            
            while (!RunUntilInputExit.stopReading)
            {

                if (!RunUntilInputExit.stopReading && Properties.NStream.DataAvailable)
                {
                   
                    Program.Monitor.BeginReading();
                    
                }
                Thread.Sleep(500);
                
            }
        }

        public static void LoopUntilInputExit(string[] args)
        {
            Properties.LastEncodedNetworkStream = null;
            //Properties.LastEncodedNetworkStream = null;
            Task.Run(()=>BeginReadingForEver());



            ReadCommandFromConsoleAndExecute.WaitForCommandInput();

            
        }
    }
}
