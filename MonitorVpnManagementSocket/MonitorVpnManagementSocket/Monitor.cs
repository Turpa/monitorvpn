﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MonitorVpnManagementSocket
{
    partial class Program
    {
        public class Monitor
        {
            public static IAsyncResult readAsyncResult;
            public static Task readAsyncTask;


            // Create and Connect socket.
            public static void ConnectMonitor()
            {
                // Properties-CLASS IpAddress and Port is set from SetAppConfigurtion.
                // Create new IPEndPoint.
                Properties.IpEndPoint = new IPEndPoint(Properties.IpAddress, Properties.Port);
                
                // Create New Socket.
                Properties.S = new Socket(SocketType.Stream,
                                            ProtocolType.Tcp);
                // Set Socket Options.
                Properties.S.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);

                // Connect Socket.
                SNConnect();
                
            }

            // Start async reading recived socket data (will loop until program stops).
            public static IAsyncResult BeginReading()
            {
                Properties.Buffer = new byte[Properties.S.Available];
                try
                {

                    readAsyncResult = Properties.NStream.BeginRead(Properties.Buffer, 0, Properties.Buffer.Length, new AsyncCallback(EndReading), Properties.NStream);
                    //Properties.NStream.BeginRead(Properties.Buffer, 0, Properties.Buffer.Length, new AsyncCallback(EndReading), Properties.NStream);
                    
                    return readAsyncResult;

                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }
                return null;
            }


            // Async reading finnish
            public static void EndReading(IAsyncResult ar)
            {

                // EncodeNetworkStream.GetEncodedNetworkStream();
                Properties.NetworkStreamNumberOfBytesRead = Properties.NStream.EndRead(ar);
                
                Properties.LastEncodedNetworkStream = EncodeNetworkStream.GetEncodedNetworkStream();

                if (Properties.ConsoleWrite)
                {
                    // Encode recived socket data, print to console.
                    Console.Write("\r{0}", Properties.LastEncodedNetworkStream);
                }

                Properties.ConsoleWrite = true;

            }

            public static void Send(string aCommand)
            {
                Properties.SendBuffer = Encoding.ASCII.GetBytes(aCommand + "\n");
                Properties.S.Send(Properties.SendBuffer, 0, Properties.SendBuffer.Length, SocketFlags.None);
            }




            public static void BeginSend(string aCommand)
            {
                 
                // Encode command from input.
                Properties.SendBuffer = Encoding.ASCII.GetBytes(aCommand + "\n");

                // Send encoded message to socket.
                Properties.NStream.BeginWrite(Properties.SendBuffer, 0, Properties.SendBuffer.Length, new AsyncCallback(BeginSendSocketCallback), Properties.NStream);

            }

            // Async send Socket data callback. (Called when task finnish)
            private static void BeginSendSocketCallback(IAsyncResult ar)
            {
                // Tells socket that it's finnished sending.
                Properties.NStream.EndWrite(ar); 
                
            }

            private static void SNConnect()
            {

                // Connect to socket
                IAsyncResult aRResult = Properties.S.BeginConnect(Properties.IpEndPoint.Address, Properties.Port, null, null);

                // Make current thread wait for 5 seconds. 
                aRResult.AsyncWaitHandle.WaitOne(5000, true);

                // check if socket connection was successfuly.
                if (Properties.S.Connected)
                {
                    // tell async connect it's finnished successful.
                    Properties.S.EndConnect(aRResult);

                    // Begin a networkstream to socket.
                    Properties.NStream = new NetworkStream(Properties.S);

                    // Connected.
                    Properties.Connected = true;
                }

                // check if socket connection was unsuccessfuly connected.
                else
                {
                    // close socket.
                    Properties.S.Close();

                    // Inform user.
                    Console.Write("Failed to connect to server on {0}", Properties.IpEndPoint);
                    Console.Read();
                    // Terminate application.
                    Environment.Exit(1);
                }


            }
            
        }
        

        

    }
}

