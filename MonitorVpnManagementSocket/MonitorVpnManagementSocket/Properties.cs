﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MonitorVpnManagementSocket
{
    class Properties
    {
        private static bool consoleWrite;
        private static StringBuilder lastEncodedNetworkStream;
        private static NetworkStream nStream;
        private static int networkStreamNumberOfBytesRead;
        private static Socket s;
        private static IPAddress ipAddress;
        private static IPEndPoint ipEndPoint;
        private static List<Client> listOfClientsConnected;
        private static byte[] buffer;
        private static byte[] sendBuffer;
        private static bool connected;
        private static int port;
        private static string user;
        private static string password;


        public static List<Client> ListOfClientsConnected
        {
            get { return listOfClientsConnected; }
            set
            {
                if(value == null)
                {
                    //update sqlserver - remove all connections
                    listOfClientsConnected = new List<Client>();
                }
                if (listOfClientsConnected == null)
                {
                    listOfClientsConnected = value;

                }
                else
                {
                    //i1 x1 2 3 4 i2 x0 1 2 3

                    // 1, 2
                    // 3




                    for (var i = 0; i < listOfClientsConnected.Count; i++)
                    {

                        for (var x = 0; x < value.Count; x++)
                        {
                            if (listOfClientsConnected[i].CommonName == value[x].CommonName)
                            {
                                // value[x] already connected 
                                // update new values. time, bandwith etc.
                                listOfClientsConnected[i].PrintClient();
                                listOfClientsConnected[i].BytesSent = value[x].BytesSent;
                                listOfClientsConnected[i].BytesReceived = value[x].BytesReceived;
                                listOfClientsConnected[i].ConnectedSince = value[x].ConnectedSince;
                                listOfClientsConnected[i].PrintClient();

                                break;
                            }
                            else if(x==value.Count - 1)
                            {
                                // i discconect
                                if (i == listOfClientsConnected.Count - 1)
                                {
                                    //x connect.
                                }
                            }
                        }
                    }
                }
            }



                
            
        }



        public static bool ConsoleWrite
        {
            get; set;
        } = true;

        public static StringBuilder LastEncodedNetworkStream
        {
            get { return lastEncodedNetworkStream; }
            set
            {
                lastEncodedNetworkStream = value;
            }
        }

        public static byte[] Buffer
        {
            get { return buffer; }
            set
            {
                buffer = value;
            }
        }
        public static byte[] SendBuffer
        {
            get { return sendBuffer; }
            set
            {
                sendBuffer = value;
            }
        }
        public static bool Connected
        {
            get { return connected; }
            set
            {
                connected = value;
            }
        }
        public static IPEndPoint IpEndPoint
        {
            get { return ipEndPoint; }
            set
            {
                ipEndPoint = value;
            }
        }
        public static NetworkStream NStream
        {
            get { return nStream; }
            set
            {
                nStream = value;
            }
        }
        public static int NetworkStreamNumberOfBytesRead
        {
            get { return networkStreamNumberOfBytesRead; }
            set
            {
                networkStreamNumberOfBytesRead = value;
            }
        }
        public static Socket S
        {
            get { return s; }
            set
            {
                
                s = value;
                
            }
        }

        public static IPAddress IpAddress
        {
            get { return ipAddress; }
            set
            {
                ipAddress = value;
            }
        }
        public static int Port
        {
            get { return port; }
            set
            {
                if (value > 0 && value <= 65535)
                {
                    port = value;
                }
                else
                {
                    Console.WriteLine("Error App.config:Invalid port {0}!\n\rValid port range between 1-65535.\n\nTerminating Application.\nPress < Any key > to continue.", value);
                    Console.Read();
                    Environment.Exit(1);
                }
            }
        }
    }
}
