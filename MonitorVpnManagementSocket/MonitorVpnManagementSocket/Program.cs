﻿using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System;

namespace MonitorVpnManagementSocket
{
    partial class Program
    {
        static void Main(string[] args)
        {
            // Get information from App.config and set properties.(IP and Port)
            SetAppConfiguration.SetAppConfigurations();
            // Create socket and connect to ip and port.
            Monitor.ConnectMonitor();
            // Start async reading recived socket data (will loop until program stops).
            //Monitor.BeginReading();



            // Runs until "exit" is typed, and program stops. 
            RunUntilInputExit.LoopUntilInputExit(args); /*.GetAwaiter().GetResult();*/


            

        }
    }
}
