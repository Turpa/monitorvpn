﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MonitorVpnManagementSocket
{
    class EncodeNetworkStream
    {
        public static StringBuilder GetEncodedNetworkStream()
        {
            StringBuilder sBuilder = new StringBuilder();
            Properties.LastEncodedNetworkStream = sBuilder.AppendFormat("{0}", Encoding.ASCII.GetString(Properties.Buffer, 0, Properties.Buffer.Length));
            return Properties.LastEncodedNetworkStream;
        }

    }
}
